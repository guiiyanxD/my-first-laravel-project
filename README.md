# my first laravel project

I'm start learning to use laravel in Tech Home - Bolivia
once you have installed PHP(xampp or lampp), Composer and Laravel on your computer you are able
to start.

First, to create a project on Windows 10, open the CMD window (by pressing win+r)
then go to:
C:\xampp\htdocs\
then write:
`composer create-project --prefer-dist laravel/laravel your-project-name "5.8.*"`
so that your project is created by composer.

After that, you have to move to your poject directory (using CMD) and start using artosan command
`php artisan serve` 
and go to localhost:8000 and you'll see the Laravel's homepage